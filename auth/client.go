package auth

import (
	"net/http"
	"net/url"
	"path"
	"time"

	"gitlab.com/distributed_lab/logan/v3/errors"
)

func throttle() chan time.Time {
	burst := 2 << 10
	ch := make(chan time.Time, burst)

	go func() {
		tick := time.NewTicker(1 * time.Second)
		// prefill buffer
		for i := 0; i < burst; i++ {
			ch <- time.Now()
		}
		for {
			ch <- <-tick.C
		}
	}()
	return ch
}

type Client struct {
	base     *url.URL
	throttle chan time.Time
	// Client must only be called from Client.Do() methods only
	client *http.Client

	auth *BasicAuth
}

func (c *Client) Do(request *http.Request) (*http.Response, error) {
	<-c.throttle

	// ensure content-type just in case
	request.Header.Set("content-type", "application/json")
	if c.auth != nil {
		request.SetBasicAuth(c.auth.Username, c.auth.Password)
	}

	response, err := c.client.Do(request)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to perform http request")
	}

	return response, nil

}

func NewClient(client *http.Client, base *url.URL) *Client {
	return &Client{
		base:     base,
		throttle: throttle(),
		client:   client,
	}
}

func (c *Client) WithAuth(auth BasicAuth) *Client {
	return &Client{
		base:     c.base,
		auth:     &auth,
		throttle: c.throttle,
		client:   http.DefaultClient,
	}
}

func (c *Client) Resolve(endpoint *url.URL) (string, error) {
	u := *c.base
	basePath := u.Path

	if basePath != "" {
		endpoint.Path = path.Join(basePath, endpoint.Path)
		u.Path = ""
	}

	resolved := u.ResolveReference(endpoint)
	return resolved.String(), nil
}
