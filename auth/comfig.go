package auth

import (
	"net/http"
	"net/url"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type AuthClienter interface {
	AuthClient() *Client
}

type clienter struct {
	getter kv.Getter
	once   comfig.Once
}

func NewAuthClienter(getter kv.Getter) *clienter {
	return &clienter{
		getter: getter,
	}
}

func (h *clienter) AuthClient() *Client {
	return h.once.Do(func() interface{} {
		var config struct {
			Endpoint *url.URL `fig:"endpoint,required"`
			Username *string  `fig:"username"`
			Password *string  `fig:"password"`
		}

		err := figure.
			Out(&config).
			With(figure.BaseHooks).
			From(kv.MustGetStringMap(h.getter, "auth_client")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out client"))
		}

		cli := NewClient(http.DefaultClient, config.Endpoint)
		if config.Username != nil && config.Password != nil {
			cli = cli.WithAuth(
				BasicAuth{
					Username: *config.Username,
					Password: *config.Password,
				})

		}
		return cli
	}).(*Client)
}
